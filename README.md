# ROOT / Python / Side by Side md output project

want to use jupyter hub and pyroot to get a better ROOT user experience

## Setup

### PyROOT

Note that numpy must be installed for each python interpreter for that interpreter to be found.

This project is to use python3, not python 2.\*

Make sure numpy is installed for python3

```bash
pip3 install numpy
```



### Pre setup, building ROOT correctly

To ensure PyROOT is enabled for a ROOT installation, during build, check
```
- Enabled support for: ... pyroot ...
```
To ensure it lists `pyroot` as an option. If it doesn't, there is something wrong and it must be enabled.

Including the cmake option `-Dpyroot=ON` will explicitly attempt to enable PyROOT, and worked in my case where pyroot wasn't showing up before.

Run cmake with the following build options.
``` bash
cmake -DCMAKE_INSTALL_PREFIX=../install -DCXX_STANDARD=14 -DPython3_EXECUTABLE=/usr/bin/python3 -DPython2_EXECUTABLE=/usr/bin/python -Dpyroot=ON ../root_src
```

install

```bash
cmake --build . --target install -- -j8
```

Running python3 and importing, the library should import without error.

**PyROOT works**

###  Jupyter Notebook

[following this article](https://www.digitalocean.com/community/tutorials/how-to-set-up-jupyter-notebook-with-python-3-on-ubuntu-18-04)

Create a python virtual environment to act as the jupyter notebook environment.

Make sure `virtenv` and `metakernel` are installed
```bash
sudo -H pip3 install --upgrade pip
sudo -H pip3 install virtualenv
sudo pip3 install metakernel
```

Make a project directory where the virtenv will be and to keep proj files. Calling mine `pyroot_jupyter_env`.

Before installing jupyter, need to activate the virtual environment.
```bash
source pyroot_jupyter_env/bin/activate
```
An indicator to the left of the terminal input should indicate the environment switched to.


# TODO

- Handle custom ROOT event classes (SimuEvent)
- host the Jupyter notebook on gitlab through CI/CD and access the notebook from there instead of locally.
